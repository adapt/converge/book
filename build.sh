#!/bin/sh

export CARGO_HOME=$HOME/.cargo
export PATH=$PATH:$CARGO_HOME/bin

which mdbook-svgbob || {
	which cargo || {
		curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs -o rustup-init.sh && bash -- rustup-init.sh -y
	} && cargo install mdbook mdbook-svgbob
}

mdbook build
