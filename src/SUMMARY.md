# Summary

[Introduction](introduction.md)

# The Operator's Handbook

- [Installation]()
- [A Brief Tour](ops/tour.md)
- [Philosophy and Approach](ops/philosophy.md)
- [Peering]()
  - [Peer Privacy](ops/peer-privacy.md)
  - [Peer History]()
- [Pinning Content](ops/pinning.md)
- [Handling Conflicts]()
- [Other Applications]()

# Application Development

- [Paradigm](app/paradigm.md)
- [Overall Architecture]()
- [Core Data Model]()

# Converge Protocols and Internals

- [Motivations and Goals](lib/motivation.md)
- [Implementing the Core Data Model]()
  - [Blobs](lib/blobs.md)
  - [Versions](lib/versions.md)
  - [Braids](lib/braids.md)
- [Protocols]()
  - [Distribution Protocol](lib/protocols/distribution.md)
  - [Introduction Protocol](lib/protocols/introduction.md)
  - [Persistent Fully-Encrypted Transport Version 0](lib/protocols/pfetv0.md)
- [Databases](lib/database.md)

# The Cryptographer's Guide

- [Cryptographic Requirements](crypto/requirements.md)
  - [Hash Functions](crypto/hash.md)
  - [Deterministic Signatures](crypto/signature.md)
  - [Deterministic Authenticated Encryption with Associated Data](crypto/daead.md)
- [Use in the Core Data Model]()
  - [Blobs](crypto/blobs.md)
  - [Versions](crypto/versions.md)
  - [Braids](crypto/braids.md)
- [Use in the Protocol]()
- [Cryptographic Selections](crypto/selections.md)
