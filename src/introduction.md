# Introduction

Personal computing began as scattered machines,
data ferried from one to the other by hand on floppy disks or tapes.
Interconnection between computers were slow, and grew slower with distance.
These constraints ensured that the applications and data were in the operator's control,
and could not be easily taken away by application providers.
Though many providers tried, that power ultimately rested with the operators.

The rise of the Internet changed those constraints,
and likewise the ecosystem of software within it.
An enormous amount of the software now runs on computers in a datacenter somewhere,
leaving us to interact with but a thin skin.
The power, for the most part, has shifted to application providers.

Operators gave up control of their data and applications for many reasons,
but the most significant one is the ease of collaboration.
It is orders of magnitude easier to work on a centralized document than
to send different versions of that document around and keep track of changes.
Distributed version control systems provide a theoretical solution,
but in practice their use is mostly restricted to software development
and fails to provide anything like real-time collaboration.

Converge is a distributed data network for collaborative local applications.

- Data is always encrypted, providing privacy regardless of where it is stored or how it is sent.

- Just enough metadata is left unencrypted to efficiently synchronize the encrypted data.

- Multiple people can edit the same data, then reconcile any conflicts after synchronization.

- Synchronization can work over both network connections and storage media.

It is designed to carry all sorts of data, from short text messages to giant videos,
from rapidly changing multiple-access databases to append-only archives.
It is designed to work with existing relationships, between friends and family, colleagues, and collaborators,
rather than only with anonymous people on the Internet (though it works for that too).

```bob
                      .---------.          .---------.
                     | user apps |        | user apps |
                      '---------'          '---------'
                         ^ ^ ^                ^ ^ ^
                         | | |                | | |
                         v v v                v v v
                      .---------.           .-------.
  .----------.        | alice's |           | bob's |        .----------.
 | filesystem |<----->|  cache  |<--------->| cache |<----->| filesystem |
  '----------'        '---------'           '-------'        '----------'
                             ^                 ^
                             |    .-------.    |
                              '-->| cloud |<--'   .----------.
                                  | store +----->| web server |
                                  '-------'       '----------'
```

This manual is intended to be a complete guide to the core components of Converge.
It is divided into four sections:
operating Converge and using the file system,
developing and building applications with the framework,
implementing the framework and protocols,
and finally the cryptography and how the security and privacy guarantees are met.
