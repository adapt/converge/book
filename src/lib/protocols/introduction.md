# Introduction Protocol

The [Transport Protocol] requires that the peers have two ephemeral shared keys,
one for authentication and one for encryption,
and each have an ephemeral public-private keypair to ratchet the connection.

This protocol provides the means to establish these.
