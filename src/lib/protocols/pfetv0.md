# Persistent Fully-Encrypted Transport Version 0

PFETv0 provides a persitant multiplexed stream transport between two peers.
While it is broadly inspired by [QUIC], it differs from it in several important ways:

1. It may be encapsulated by both datagram and stream-oriented protocols.

2. Every payload sent is indistinguishable from random.

3. Rather than between a client and a server, it is between two peers.

3. A connection persists indefinitely between those peers.
   Both ends may change addresses and encapsulations without disconnecting.
   The peers must have access to non-volitile storage to persist keys.

4. There is no mechanism within the protocol to start a new connection.
   Peers must be introduced to each other outside the transport protocol in order to communicate.

## Outer Packet Format

| Bytes | Description            |
| ----: | ---------------------- |
|    32 | Authentication code.   |
|  rest | Ciphertext.            |

How the length of a packet is determined varies by the type of encapsulation.

- In datagram-oriented protocols, a packet fills the entire datagram.
- In a stream-oriented protocol, the size of each packet is predetermined.
  At the initialization of each stream it is set to 256 bytes,
  and then may be changed with a [Set Packet Length](#set-packet-length) frame.

The authentication code must pass before parsing any packet

## Ciphertext

| Bytes | Description            |
| ----: | ---------------------- |
|    24 | Random Nonce.          |
|  rest | XChaCha8 ciphertext.   |

## Inner Packet Format

The inner packet, the plaintext, is made up of a header and one or more frames.
The first byte of each frame is indicates the frame type,
followed by a variable length quantity indicating the length of that frame.
Frames are contained within a single packet.

| Bytes | Description            |
| ----: | ---------------------- |
|     3 | Packet Number          |
|  rest | Frames                 |

## Frame Types

### Set Packet Length

Set the length of packets in a stream encapsulation.
In a multi-stream encapsulation, it applies only to the stream the packet is in.
In a datagram encapsulation the presence of this frame type invalidates the packet.

| Bytes | Description            |
| ----: | ---------------------- |
|     1 | 0, Frame Type          |
|   vlq | Future Packet Length   |

### Acknowledge

Acknowledgement frames sent by Alice to Bob indicate which of Bob's packets were received.
Bob will then transmit any stream or cryptographic data held in those packets in new packets.

| Bytes | Description            |
| ----: | ---------------------- |
|     1 | 1, Frame type.         |
|     3 | Latest Acknowledged    |
|   vlq | Acknowledgement Delay  |
|   vlq | n, Count of Counts     |
|  1+2n | Acknowledgement Counts |

#### Latest Acknowledged

The most recent packet number being acknowledged.

#### Acknowledgement Delay

The time in microseconds between receiving the latest acknowledged packet and sending this packet.
This count must only include delays introduced by the endpoint application.

#### Acknowledgement Counts

Alternating counts of acknowledgement and gaps, each 1 byte long.

The first number includes the latest acknowledgement, so must be greater than 0.

Acknowledgements may only be set, not cleared,
so gaps may include packets that have previously been acknowledged.

In a multi-stream encpasulation packets may be acknowledged over any stream.

### Key Update

A key update sends a partial update of this peer's KEM or Diffie-Hellman key.

| Bytes | Description            |
| ----: | ---------------------- |
|     1 | 16 \| flags            |
|   vlq | Public Key Data Offset |
|   vlq | Length of Frame        |
|  rest | Public Key Data        |

#### Flags

| Bit | Description            |
| --: | ---------------------- |
|   0 | Initial Update Packet  |
|   1 | Final Update Packet    |
|   2 | Type of Update         |

The `initial` bit is set when this packet contains the first byte in a key update.
The `final` bit is set when this packet contains the last byte in a key update.

| Type | Description            |
| ---: | ---------------------- |
|    0 | Key Encapsulation Key  |
|    1 | Diffie-Hellman Key     |

#### Public Key Data Offset

### Stream Data

| Bytes | Condition | Value | Description                                      |
| ----: | :-------- | ----: | ------------------------------------------------ |
|     1 |           |240-248| Field Type \| `last` \|\| `initial` \|\| `final` |
|   vlq |           |  `id` | Stream Identifier                                |
|   vlq |           | `off` | Stream Data Offset                               |
|   vlq | ¬`last`   |   `n` | Length of Data                                   |
|   `n` |           |       | Stream Data                                      |

#### Flags

| Bit | Name      | Description                                    |
| --: | :-------- | ---------------------------------------------- |
|   0 | `final`   | This frame is the final frame in a stream.     |
|   1 | `initial` | This frame is the first frame in a new stream. |
|   2 | `last`    | This frame is the last frame in this packet.   |

### Terminate Stream

| Bytes | Description            |
| ----: | ---------------------- |
|     1 | 252 \| `yours`         |
|   vlq | Stream Identifier      |
|   vlq | Application Error      |

This unconditionally terminates a stream, providing an application layer error code.

`yours` is a one bit value indicating whether the identifier refers to the receiving host's streams.
That is, frames of type 252 terminate the sender's streams,
and frames of type 253 terminate streams that the receiver is sending.

### Terminate Connection

| Bytes | Condition | Value | Description                                      |
| ----: | :-------- | ----: | ------------------------------------------------ |
|     1 |           |   254 | Field Type                                       |
|   vlq |           |       | Application Error                                |

This unconditionally terminates the connection,
providing an application-specific error as explanation.
Both sides must delete all keys, including from non-volitile storage,
and no further communication on this connection is valid.

The sender should wait for their peer to acknowledge a packet containing this frame, resending as necessary.
After sending this frame however, the sender must not send any frames other than padding.
If the peer continues to send packets and does not acknowledge any packet containing this frame,
the sender should then procede to fully terminate the connection,
though it may keep the MAC key to detect further packets of this connection.

### Padding

| Bytes | Description            |
| ----: | ---------------------- |
|     1 | 255, Frame type.       |
|  rest | ignored                |

Padding may be used to reach a minimum packet size or fill out a packet to a fixed size to obscure length analysis.
Padding always consumes the rest of the packet.
