# Braids

Braids are the abstract objects made of a braided stream of [versions](./versions.md).
[Cryptographically] speaking, braids are the set of versions sharing the same verification key,
from an algorithmic perspective a braid is the ever-growing directed acyclic graph with multiple entry-points formed by those versions.

This provides the illusion of a concurrently mutable data structure built on immutable data.
The set of entry points into this graph marks the current versions.
That is, `d` and `e` are the current versions in the graph below:

```bob
     .---.
     | a |
     '---'
      ^ ^
     /   \
  .-+-. .-+-.
  | b | | c |
  '---' '---'
   ^   ^  ^
   |  /   |
  .+-+. .-+-.
  | d | | e |
  '---' '---'
```


[Cryptographically]: ../crypto/braids.md