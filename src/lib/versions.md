# Versions

Versions provide read-write storage.

They contain some ciphertext, a set of references to other Objects, and an ordered set of references to parent Versions.

```rust
struct Version {
  data: Ciphertext,
  refs: BTreeSet<Reference>,
  vers: Vec<VersionSignature>,
}
```

## Version Serialization

Versions are serialized using [atlv], as an array of those three fields, using the tag 1.
The references must be listed in lexicographic order.
As an example, a 128 byte ciphertext with a single reference to another braid and a single parent:

```
offset  bytes  description
00:     07     the tag (1) and type (array) of the version
01:     03     the number of elements in the array
02:     01     the tag (0) and type (binary) of the ciphertext
03:     01 00  the length of the ciphertext (128 bytes)
05…84:  ...    the ciphertext
85:     23     the tag (8) and type (array) of the reference vector
86:     01     the length of the reference vector
87:     4a     the tag (18) and type (union) of a Braid public key reference
88:     01 20  the tag (0) and type (binary) and length (32 bytes) of a blake3-schnorr-ristretto255 public key
8a…a9:  ...    the content of the blake3-schnorr-ristretto255 public key
aa:     47     the tag (17) and type (array) of the parents vector
ab:     01     the length of the parents vector
ac:     01 40  the tag (0) and type (binary) and length (64 bytes) of a blake3-schnorr-ristretto255 signature
ad…ed:  ...    the content of the blake3-schnorr-ristretto255 signature
```

[atlv]: https://adpt.dev/tools/atlv
