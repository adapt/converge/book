# Blobs

Blobs provide read-only content addressed encrypted storage.

They contain some ciphertext and a set of references to other objects.

```rust
struct Blob {
  data: Ciphertext,
  refs: BTreeSet<Reference>,
};
```

## Serialization

Blobs are serialized using [atlv], as an array of those two items, using the tag 0.
The references must be listed in lexicographic order.
As an example, a 128 byte ciphertext with a single reference to another blob:

```
offset  bytes  description
00:     03     the tag (0) and type (array) of the blob
01:     02     the number of elements in the array
02:     01     the tag (0) and type (binary) of the ciphertext
03:     01 00  the length of the ciphertext (128 bytes)
05…84:  ...    the ciphertext
85:     23     the tag (8) and type (array) of the reference vector
86:     01     the length of the reference vector
87:     42     the tag (16) and type (union) of a Blob Hash reference
88:     01 20  the tag (0) and type (binary) and length (32 bytes) of a blake3 hash
8a…a9:  ...    the content of the blake3 hash
```

[atlv]: https://adpt.dev/tools/atlv
