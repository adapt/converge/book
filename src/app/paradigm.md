# Paradigm

There are four broad problems in computing:

- Communication: getting data from one computer to another.
- Interface: getting data into and out of a computer.
- Storage: holding on to some data for a while.
- Compute: turning some data into different data.

Converge works at the junction of storage and communication.
These have traditionally been considered quite distinct.
Even network file systems don't quite bridge the gap;
they provide access to files on other computers,
and as such are much more communication than storage.

With converge, which computer in a network is holding the data is incidental,
and any request for the data does not mention it.
There are implications of the data being held on one computer or another,
and mechanisms to keep a copy of some data on a particular computer,
but every computer is an equally valid and authoritative host for any piece of data.

Yet converge can be used to communicate -
updates are propagated through the network from interested host to interested host.
This enables tremendous fan-out for multicast-like communications,
while also providing a level of anonymity for both readers and writers.
Perhaps the upstream computer originated the data,
or perhaps it just got it from elsewhere,
perhaps the downstream computer will present the data to its user,
or perhaps it will send it on to another computer.