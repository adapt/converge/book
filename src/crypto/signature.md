# Deterministic Signatures

Converge requires a [signature scheme] that is a [self-certifying public key scheme](requirements.md#self-certifying-public-key-scheme).
Deterministic signature schemes have two functions, beyond those provided by a public key scheme.

```haskell
-- Signing
sign :: Domain -> SecretKey -> Message -> Signature
-- Verifying
verify :: Domain -> PublicKey -> Message -> Signature -> Either Error ()
```

It must be infeasible to:

- derive information about a secret key for any other domain and master key from a secret key

- create a secret key that matches a given public key, even given a vast number of signatures produced with that secret key

- find two domain-message-key tuples with the same signature

- find a domain-message-key tuple that results in a particular signature

- transform a signature on any message into one that's valid on a chosen domain and message with the same key

- produce a signature for a particular public key without access to the corresponding secret key

  note that this does not exclude all malleability.
  in particular, enabling threshold, one-of-many, and all-of-many signatures is encouraged.

[signature scheme]: https://en.wikipedia.org/wiki/Digital_signature
