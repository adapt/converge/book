# Hash Functions

Converge requires a [hash function] that takes a domain and message and produces a fixed length output, the digest.

```haskell
-- Hashing
hash :: Domain -> Message -> Digest
```

It must be infeasible to:

- find two domain-message pairs that hash to the same digest - [collision resistance].

- find a domain-message pair that hashes to a particular digest - [preimage resistance].

- transform a digest of one domain-message pair into that of another domain-message pair without access to the target domain and message.

[hash function]: https://en.wikipedia.org/wiki/Cryptographic_hash_function
[collision resistance]: https://en.wikipedia.org/wiki/Collision_resistance
[preimage resistance]: https://en.wikipedia.org/wiki/Preimage_attack
