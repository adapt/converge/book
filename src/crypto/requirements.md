# Cryptographic Requirements

All of Converge's cryptographic functions are parameterized by a domain string -
a statically defined byte string that expresses how the primitive is being used.
The general principle is that results of these functions in one domain must not be valid,
or transformable to be valid, in other domains.

## Public Key Scheme

A public key cryptographic scheme is a class of schemes that utilize a secret key and a public key.

```haskell
-- Key Derivation
from_master :: Domain -> MasterKey -> SecretKey
-- Public Key Derivation
to_public :: SecretKey -> PublicKey
```

## Self-Certifying Public Key Scheme

Some public key cryptographic schemes can produce self-certifying certificates,
that is they provide these additional functions:

```haskell
-- Key Derivation
from_master_certifiable :: Domain -> MasterKey -> SecretKey
-- Certficate Production
issue_certficate :: Domain -> MasterKey -> T -> Certificate T
-- Certficate Verification
verify_certficate :: PublicKey -> Certificate T -> Either Error T
```

Where the certificates cannot be created from secret keys, but require the master key.
