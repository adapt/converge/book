# Braids

[Braids] are abstract objects defined as the set of versions that share the same signature keys.

[Braids]: ../lib/braids.md

## Desired Properties

Braids provide the illusion of a concurrently mutable data structure built on immutable data.
There are thus four types of actors working with that data structure:

- fetchers can fetch the versions that make up a braid, verify that those versions were made by writers, and see the braids' public data
- readers can fetch, and read the versions' secret data
- writers can create new versions
- administrators can end the braid and rotate the keys, forming a new braid to replace it

## Key Derivation

Both the shared encryption key and secret signing key are derived from a master key,
which is generated from a high-entropy source.

The shared key is derived with the domain "Converge Braid Shared Key".

The secret signing key is derived with the domain "Converge Braid Signing Key",
and should be derived in a way that can provide a rotation certificate,
but may be derived directly from the master key if the braid is short lived.

## References

A braid is referenced by the public key used to verify them.
That typically is taken to mean the latest versions on the braid,
but may refer to the entire braid.