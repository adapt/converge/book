# Deterministic Authenticated Encryption with Associated Data

Symmetric encryption obscures a plaintext as a ciphertext with a key, and provides a function to turn the ciphertext back into a plaintext with that key.
Authenticated Encryption additionally provides strong evidence that the ciphertext was not modified in transit.
Authenticated Encryption with Associated Data further provides strong evidence that additional unencrypted data was not modified in transit.
Deterministic Authenticated Encryption with Associated Data guarantees a one-to-one relationship between plaintexts and ciphertexts for a given shared key and associated data.

Converge requires Deterministic Authenticated Encryption with Associated Data with four functions:

```haskell
-- Key Derivation
from_master :: Domain -> MasterKey -> SharedKey
-- Convergent Key Derivation
from_plaintext :: Domain -> ConvergenceDomain -> Plaintext -> AssociatedData -> SharedKey
-- Encryption
encrypt :: Domain -> SharedKey -> Plaintext -> AssociatedData -> Ciphertext
-- Decryption
decrypt :: Domain -> SharedKey -> Ciphertext -> AssociatedData -> Either Error Plaintext
```
