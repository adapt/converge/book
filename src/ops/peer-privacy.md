# Peer Privacy

When computers talk to each other across the Internet on our behalf,
they usually reveal unintended information.
Most importantly exchanging messages with someone else's computer reveals your computer's Internet address,
which conveys some information about your physical location (to some extent),
to that computer and all the computers along the way.

However, some servers on the Internet will exchange messages on behalf of your computer.
A single such servers, often known as a proxy, will hide your location,
albeit not from the proxy itself.
But with a series of proxies, typically at least three,
each proxy knows only the address of the previous and next computer to talk to.
This disguises who exchanging messages with whom much more strongly.
This is generally known as private routing, and can take a [number of forms](lib/location-privacy.md).

During peering you set how private you want your location to be from this peer:

1. No location privacy, allow the peer to connect directly your device.
2. Basic location privacy, use private routing when connecting over the Internet,
   but allow direct connections when both peers are on the same network.
3. Complete location privacy, always use private routing with this peer.

The actual communication method used will be the maximum of what each peer is set for each other.
However, the location information exposed to a peer is always at the level set.

