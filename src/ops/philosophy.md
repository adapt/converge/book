# Philosophy and Approach

Some aspects of Converge work a little differently than you may be used to, in particular in the realm of sharing.
There are principles at work here, and in this chapter we explore those principles and their implications.

- the wishes of a device's operator override any others' wishes for what happens to that device
- avoid functionality that cannot be guaranteed or verified
- present the full functionality of the underlying system to the operator

## Operator Control

We have firmly transitioned to the world where almost all devices have a primary operator,
typically the person who would say "that's my device".
The device may also have occasional other people who use it,
but when we speak of the wishes of the operator overriding all others,
it is that primary operator of which we speak.

There is some difficulty in determining who that operator is - but assuming we can,
Converge should perform the requests of that operator to the best of its abilities.
Some would assert that others' goals for that device should also be taken into account,
the manufacturer, the cell service provider, the government, or their spouse,
or even me, writing Converge to run on their device.
We heartily reject that assertion.
Tools must serve their operators first, and only those of their operators choosing after.

## Verifiable Functionality

Converge does not implement any functionality that cannot be verified by the same actor.
Most importantly, there is no mechanism to unshare what has already been shared,
or to delete data from someone else's system.
Sure, you can have your device refuse to hand over the data (but they still have the decryption key),
or if they give you access to write to their files you can delete a file from a file tree (but it's still there in the history of the file tree).

## Complete Operator Interfaces

If there's functionality in the underlying system, that functionality should be presented to the operator,
and functionality that is not in the underlying system should not be presented to the operator.

Likely the most startling implication of this is the [Peer History](peer-history.md) functionality -
here you see a compilation of all the information your peer has leaked about itself to you,
and that you have leaked to your peer about yourself.
