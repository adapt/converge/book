# A Brief Tour

When you first install Converge on your phone,
the primary change you'll see is a new shared drive that you can add files and directories to.
Of course, until you establish peering with another device you can't share anything!
So you install Converge on your laptop, and select peer on both devices.

Peering will ask you whether you're peering your own device or someone else's,
prompt your for access to your secret storage,
and then display a QR code.
You select your own device, authenticate the access, scan both QR codes, and now are peered.
Any directories or files you added to the shared storage on one device will now show up on the other,
though the actual data won't copy until it's requested.

So you make a directory for pictures, select the directory and ask it to pin it to the phone and the laptop.
This automatically keeps a copy of the directory and all its subdirectories on both devices, preemptively copying everything.
You decide that you don't need to keep a copy of *every photo* on your phone, and so select a couple subdirectories and unpin them,
most of the directory tree remains pinned, but those don't.

Then you decide to add your desktop.
Peering your desktop goes much the same as your phone and laptop,
except for one difference - your desktop doesn't have a camera.
You scan the QR code from your desktop with your phone,
and then your phone shows you a bunch of words to type in:
`let food note buy ring album time fire`.
You type them in and now both your phone and laptop are peered to your desktop.

## Secure Backup

You want to keep an offsite backup of all your data in Converge,
so you sign up with a cloud provider that offers storage.
They provide you a link, and when you click on it Converge opens.
Converge informs you what capabilities they've given you,
probably fetch, write, subscribe, and pin,
asks whether you want to accept those,
whether you want to reveal your IP address (and potentially location) to them,
and what name you want to give your new peer.

You'll then be able to pin files, directories, and other objects to them,
backing up your data without revealing the contents of those objects.

## Friends, Family, Colleagues, Collaborators

