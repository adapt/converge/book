# Some Notes on Pinning

Objects are usually pinned to one or more devices,
starting out with the device that the object was created on.
This means that that device is responsible for keeping a copy of the latest versions of that object available,

Most pinning applies recursively -
if you pin a directory to a device, all the files and directories within will also get *implicitly* pinned to that device.
Implicit pinning can be interrupted by [unpinning](#unpinning),
and [light pinning](#light-pinning) doesn't apply recursively at all.

## Unpinning

Unpinning prevents an *explicit* pin from applying to a particular object.
For example, if you have your music directory pinned to your phone,
you might unpin directories for artists or albums that you don't want access to offline.

## Light Pinning

Light pinning keeps a copy of the latest versions of an object,
but doesn't apply it recursively.
This is useful for keeping a directory listing up to date on a device,
without automatically pulling everything inside that directory.

Light pinning is used by default on the root directory that is shared between all your devices,
and the peer directories that you share with a connection.

## Backup

While a pin makes a device responsible for keeping copies of the latest versions,
a backup additionally makes a device responsible for keeping copies of historical versions.

Backups may either be partial or complete.
Complete backups are what they sound like: keep every single version.
Partial backups are more application-dependent.
For the file system, it typically means keeping a version for each day.

## Number of Copies

Pinning can specify how many copies you want stored on a device.
Many devices only store one copy, because they only have one physical drive to store it one.
However, your phone might have both its internal storage and an SD card, so it could store two copies,
and a server likely has many hard drives, and could in theory store that many copies.
This protects against losing the data to a drive failure.

In many cases, objects are pinned to multiple devices anyway,
which protects against much more catastrophic failures,
so pinning multiple copies isn't always beneficial.

However, pinning multiple copies can be very useful for backups,
as you're less likely to keep historical versions on multiple devices.
The other place it shines is that a Converge-aware storage provider should distribute multi-copy pins over multiple data centers,
which can help protect against regional disasters.
Of course, pinning one copy to a couple distant friends' servers accomplishes the same thing,
with less centralized failure scenarios.

## References and Acknowledgements

The pinning and backup functionality is using the [pin and repin](../lib/protocols/distribution.md#pin) capabilities in the protocol.

The term pinning in this sense was borrowed from [IPFS].

[IPFS]: https://ipfs.io
